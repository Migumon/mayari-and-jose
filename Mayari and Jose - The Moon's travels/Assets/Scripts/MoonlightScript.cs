﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonlightScript : MonoBehaviour {

	private SpriteChanger player;

	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<SpriteChanger> ();
	}
		
	void OnTriggerEnter2D(){
		player.MoonlightCount += 1;
		Destroy(this.gameObject);
	}
}
