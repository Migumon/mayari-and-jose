﻿using UnityEngine;
using System.Collections;

public class MovementBehavior : MonoBehaviour{

	public float maxSpeed = 2;
	public float speed = 30f;
	public float jumpPower = 150f;

	private Rigidbody2D rb2d;

	void Start()
	{
		rb2d = gameObject.GetComponent<Rigidbody2D>();
	}

	void Update()
	{
		if(Input.GetButtonDown("Jump"))
		{
			rb2d.AddForce(Vector2.up * jumpPower);
		}
	}

	void FixedUpdate()
	{
		float h = Input.GetAxis("Horizontal");

		rb2d.AddForce((Vector2.right * speed) * h);


		if(rb2d.velocity.x > maxSpeed)
		{
			rb2d.velocity = new Vector2(maxSpeed, rb2d.velocity.y);
		}

		if(rb2d.velocity.x < -maxSpeed)
		{
			rb2d.velocity = new Vector2(-maxSpeed, rb2d.velocity.y);
		}
	}
}