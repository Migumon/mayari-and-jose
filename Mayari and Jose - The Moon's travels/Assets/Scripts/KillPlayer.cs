﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayer : MonoBehaviour {

	private DeadOrAlive deadOrAlive;

	void Start () {
		deadOrAlive = GameObject.FindGameObjectWithTag ("Player").GetComponent<DeadOrAlive> ();
	}
	void OnTriggerEnter2D(){
		deadOrAlive.alive = false;
	}
}
