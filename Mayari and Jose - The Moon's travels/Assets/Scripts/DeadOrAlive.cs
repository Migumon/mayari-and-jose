﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeadOrAlive : MonoBehaviour {

	public bool alive;

	void Start () {
		alive = true;
	}

	void Update () {
		if(alive == false){
			Application.LoadLevel(Application.loadedLevel);
			alive = true;
		}
	}
}
