﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {

	public Transform TeleportTo;

	void OnTriggerEnter2D(Collider2D other){
		other.gameObject.transform.position = TeleportTo.position;
	}
}
