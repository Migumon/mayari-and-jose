﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteChanger : MonoBehaviour {

	public Sprite Crescent;
	public Sprite HalfMoon;
	public Sprite AlmostFull;
	public Sprite FullMoon;

	public int MoonlightCount;

	void Start () {
		MoonlightCount = 0;
	}

	void Update () {
		if(MoonlightCount == 1){this.gameObject.GetComponent<SpriteRenderer> ().sprite = Crescent;}
		if(MoonlightCount == 2){this.gameObject.GetComponent<SpriteRenderer> ().sprite = HalfMoon;}
		if(MoonlightCount == 3){this.gameObject.GetComponent<SpriteRenderer> ().sprite = AlmostFull;}
		if(MoonlightCount == 4){this.gameObject.GetComponent<SpriteRenderer> ().sprite = FullMoon;}
	}
}
